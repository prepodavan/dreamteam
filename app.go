package main

import (
	"dreamteam/handlers"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/mongo"
	"github.com/gin-gonic/gin"
	"os"

	"dreamteam/db"
)

func main() {
	defer db.Session.Close()
	router := gin.Default()

	store := mongo.NewStore(db.Sessions, 3600, true, []byte("secret"))
	router.Use(sessions.Sessions("dream-team-user", store))

	api := router.Group("/api")

	v1 := api.Group("/v1")
	v1.POST("/user",
		handlers.RequiresNameAndPass,
		handlers.CheckUserAlreadyExists,
		handlers.PostUser)

	v1.POST("/auth",
		handlers.RequiresNameAndPass,
		handlers.Auth)

	auth := v1.Group("", handlers.CheckAuth)
	{
		user := auth.Group("/user/:id", handlers.CheckUserID)
		{
			user.GET("/", handlers.GetUser)
			user.GET("/groups", handlers.GetGroups)
		}
		friends := auth.Group("/friends")
		{
			friends.GET("/", handlers.GetFriends)
			friends.GET("/requests", handlers.GetRequests)

			certainPersonActions := friends.Group("",
				handlers.RequiresPersonID,
				handlers.CheckPersonID)

			certainPersonActions.POST("/confirm", handlers.ConfirmFriendHandler)
			certainPersonActions.POST("/remove", handlers.RemoveFriendHandler)
			certainPersonActions.POST("/request", handlers.RequestForFriendship)
		}
		invites := auth.Group("/invite")
		{
			invites.POST("/",
				handlers.RequiresGroupAndPerson,
				handlers.CheckIsMember,
				handlers.CheckPersonID,
				handlers.IsFriendWithPerson,
				handlers.Invite)
			invites.GET("/", handlers.GetInvites)
			certainInvite := invites.Group("/:id", handlers.RequireInviteID)
			{
				certainInvite.POST("/deny", handlers.DenyInvite)
				certainInvite.POST("/confirm", handlers.ConfirmInvite)
			}
		}
		groupOfGroup := auth.Group("/group")
		{
			groupOfGroup.POST("/", handlers.RequiresTitleAndDesc, handlers.CreateGroup)
			certainGroup := groupOfGroup.Group("/:id", handlers.RequiresGroup)
			{
				certainGroup.GET("/", handlers.GetGroup)
				certainGroup.GET("/members", handlers.CheckIsMember, handlers.GetMembers)
				certainGroup.GET("/image", handlers.GetImage("/api/v1/img/", router))
				certainGroup.POST("/image", handlers.SetImage("./img"))
			}

		}
	}
	auth.Static("/img", "./img")

	router.Run(os.Getenv("HOST") + ":" + os.Getenv("PORT"))
}
