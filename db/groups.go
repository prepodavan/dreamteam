package db

import (
	"errors"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	CREATOR = "creator"
	MEMBER  = "member"
)

type Group struct {
	ID          bson.ObjectId `bson:"_id,omitempty"`
	Title       string        `bson:"title"`
	Description string        `bson:"description"`
}

func FindGroup(id string) (*Group, error) {
	if !bson.IsObjectIdHex(id) {
		return nil, errors.New("invalid id")
	}

	g := new(Group)

	if err := groups.FindId(bson.ObjectIdHex(id)).One(&g); err != nil {
		return nil, err
	}

	return g, nil
}

func (g *Group) Users() ([]Membership, error) {
	allMemberships, err := FindAllByGroup(g)
	if err != nil {
		return nil, err
	}

	return FilterByStatus(CONFIRMED, allMemberships), nil
}

func (g *Group) GetID() *bson.ObjectId {
	return &g.ID
}

func (g *Group) GetCollection() *mgo.Collection {
	return groups
}
