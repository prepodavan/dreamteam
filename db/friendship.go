package db

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"time"
)

const (
	PENDING   = "pending"
	CONFIRMED = "confirmed"
	REMOVED   = "removed"
)

func currentDate() int64 {
	return time.Now().Unix()
}

type Friendship struct {
	ID          bson.ObjectId `bson:"_id,omitempty"`
	SenderID    bson.ObjectId `bson:"sender,omitempty"`
	RecipientID bson.ObjectId `bson:"recipient,omitempty"`
	History     []State       `bson:"history"`
}

func FindFriendship(u1 *User, u2 *User) (*Friendship, error) {
	var query Friendship

	err := friendships.Find(bson.M{"$or": []bson.M{
		{"sender": u1.ID, "recipient": u2.ID},
		{"sender": u2.ID, "recipient": u1.ID}}}).One(&query)
	if err != nil {
		return nil, err
	}

	return &query, nil
}

func FindAll(u *User) ([]Friendship, error) {
	query := make([]Friendship, 0)

	err := friendships.Find(bson.M{"$or": []bson.M{
		{"sender": u.ID},
		{"recipient": u.ID}}}).All(&query)
	if err != nil {
		return nil, err
	}

	return query, nil
}

func (f *Friendship) Remove(by *User) error {
	return f.GetHistory().Push(REMOVED, by)
}

func insertFriendship(sender *User, recipient *User) (*Friendship, error) {
	id := bson.NewObjectId()
	history := State{PENDING, currentDate(), sender.ID}
	req := &Friendship{id, sender.ID, recipient.ID, []State{history}}

	if err := friendships.Insert(req); err != nil {
		return nil, err
	}

	return req, nil
}

func (f *Friendship) GetCollection() *mgo.Collection {
	return friendships
}

func (f *Friendship) GetID() *bson.ObjectId {
	return &f.ID
}

func (f *Friendship) GetHistory() *History {
	return &History{f, f.History}
}
