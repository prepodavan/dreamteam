package db

import (
	"github.com/globalsign/mgo/bson"
)

type State struct {
	Status    string        `bson:"status"`
	ChangedAt int64         `bson:"changed_at"`
	By        bson.ObjectId `bson:"by,omitempty"`
}

type History struct {
	Owner  Model
	States []State
}

type HasHistory interface {
	GetHistory() *History
}

func NewState(status string, by *User) *State {
	return &State{status, currentDate(), by.ID}
}

func (h *History) Push(status string, by *User) error {
	collection := h.Owner.GetCollection()
	id := h.Owner.GetID()
	state := NewState(status, by)
	return collection.UpdateId(id, bson.M{"$push": bson.M{"history": state}})
}

func (h *History) Latest() *State {
	if len(h.States) == 0 {
		return nil
	}

	latest := h.States[0]

	for _, h := range h.States {
		if h.ChangedAt > latest.ChangedAt {
			latest = h
		}
	}

	return &latest
}
