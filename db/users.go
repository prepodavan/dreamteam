package db

import (
	"errors"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type User struct {
	ID       bson.ObjectId `bson:"_id,omitempty"`
	Name     string        `bson:"name"`
	Password string        `bson:"password"`
}

func (u *User) GetID() *bson.ObjectId {
	return &u.ID
}

func (u *User) GetCollection() *mgo.Collection {
	return users
}

func CreateUser(name string, password string) (*User, error) {
	u := new(User)
	u.Name = name
	u.ID = bson.NewObjectId()
	u.Password = password

	if err := users.Insert(u); err != nil {
		return nil, err
	}

	return u, nil
}

func FindUser(id string) (*User, error) {
	if !bson.IsObjectIdHex(id) {
		return nil, errors.New("invalid id")
	}
	u := new(User)
	err := users.FindId(bson.ObjectIdHex(id)).One(&u)
	if err != nil {
		return nil, err
	}

	return u, nil
}

func FindUserByName(name string) (*User, error) {
	u := new(User)
	err := users.Find(bson.M{"name": name}).One(&u)
	if err != nil {
		return nil, err
	}

	return u, nil
}

func (u *User) RequestFriend(recipient *User) error {
	if u.ID.Hex() == recipient.ID.Hex() {
		return errors.New("invalid recipient")
	}

	request, err := FindFriendship(u, recipient)

	if err != nil {
		if err.Error() == "not found" {
			_, err = insertFriendship(u, recipient)
		}

		return err
	}

	if request.GetHistory().Latest().Status != REMOVED {
		return errors.New("request already exists")
	}

	return request.GetHistory().Push(PENDING, u)
}

func (u *User) ConfirmFriend(sender *User) error {
	req, err := FindFriendship(u, sender)
	if err != nil {
		return err
	}

	if req == nil {
		return errors.New("some error")
	}

	if req.GetHistory().Latest().Status != PENDING {
		return errors.New("nothing to confirm")
	}

	return req.GetHistory().Push(CONFIRMED, u)
}

func (u *User) RemoveFriend(friend *User) error {
	req, err := FindFriendship(u, friend)
	if err != nil {
		return err
	}

	if req == nil {
		return errors.New("some error")
	}

	if req.GetHistory().Latest().Status == REMOVED {
		return errors.New("nothing to remove")
	}

	return req.Remove(u)
}

func (u *User) Friends() ([]string, error) {
	requests, err := FindAll(u)

	if err != nil || requests == nil {
		return nil, err
	}

	ids := make([]string, 0)

	for _, req := range requests {

		if req.GetHistory().Latest().Status != CONFIRMED {
			continue
		}

		var id string

		if req.RecipientID.Hex() == u.ID.Hex() {
			id = req.SenderID.Hex()
		} else {
			id = req.RecipientID.Hex()
		}

		ids = append(ids, id)
	}

	return ids, nil
}

func (u *User) Requests() ([]map[string]string, error) {

	requests, err := FindAll(u)

	if err != nil || requests == nil {
		return nil, err
	}

	reqs := make([]map[string]string, 0)

	for _, req := range requests {

		if req.GetHistory().Latest().Status != PENDING {
			continue
		}

		stringIDReq := make(map[string]string)
		stringIDReq["sender"] = req.SenderID.Hex()
		stringIDReq["recipient"] = req.RecipientID.Hex()

		reqs = append(reqs, stringIDReq)
	}

	return reqs, nil
}

func (u *User) CreateGroup(title string, description string) (*Group, error) {
	g := new(Group)
	g.Title = title
	g.Description = description
	g.ID = bson.NewObjectId()

	err := groups.Insert(g)
	if err != nil {
		return nil, err
	}

	_, err = insertMembership(g, u, u, CREATOR, CONFIRMED)
	if err != nil {
		return nil, err
	}

	return g, nil
}

func (u *User) Groups() ([]Membership, error) {
	inviting, err := FindAllInviting(u)
	if err != nil {
		return nil, err
	}

	invited, err := FindAllInvited(u)
	if err != nil {
		return nil, err
	}

	return append(FilterByStatus(CONFIRMED, invited), FilterByStatus(CONFIRMED, inviting)...), nil
}

func (u *User) Invite(g *Group, friend *User) (*Membership, error) {
	friends, err := u.Friends()
	if err != nil {
		return nil, err
	}

	flag := false
	for _, fid := range friends {
		if fid == friend.ID.Hex() {
			flag = true
			break
		}
	}

	if !flag {
		return nil, errors.New("not a friend")
	}

	mss, err := g.Users()
	if err != nil {
		return nil, err
	}

	for _, ms := range mss {
		if ms.InvitedID.Hex() == friend.ID.Hex() || ms.InvitingID.Hex() == friend.ID.Hex() {
			return nil, errors.New("already member")
		}
	}

	var query Membership

	err = memberships.Find(bson.M{"$and": []bson.M{
		{"invited": friend.ID},
		{"inviting": u.ID},
	}}).One(&query)

	if err != nil {
		if err.Error() != "not found" {
			return nil, err
		}

		if err.Error() == "not found" {
			return insertMembership(g, u, friend, MEMBER, PENDING)
		}
	}

	if query.GetHistory().Latest().Status == PENDING {
		return nil, errors.New("already exists")
	}

	return &query, query.GetHistory().Push(PENDING, u)
}

func (u *User) DenyInvite(invite *Membership) error {
	if invite.InvitedID.Hex() != u.ID.Hex() {
		return errors.New("not found")
	}

	err := invite.Remove(u)
	if err != nil {
		return err
	}

	return nil
}

func (u *User) ConfirmInvite(invite *Membership) error {
	if invite.InvitedID.Hex() != u.ID.Hex() {
		return errors.New("not found")
	}

	invited, err := FindAllInvited(u)
	if err != nil {
		return err
	}

	for _, ms := range invited {
		if invite.GroupID.Hex() == ms.GroupID.Hex() && invite.ID.Hex() != ms.ID.Hex() {
			err = ms.Remove(u)
			if err != nil {
				return err
			}
		}
	}

	return invite.Confirm(u)
}
