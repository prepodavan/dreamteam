package db

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"log"
	"os"
)

var (
	Session     *mgo.Session
	Sessions    *mgo.Collection
	DB          *mgo.Database
	users       *mgo.Collection
	groups      *mgo.Collection
	friendships *mgo.Collection
	memberships *mgo.Collection
)

func connectionString() string {
	return os.Getenv("MONGO_HOST") + ":" + os.Getenv("MONGO_PORT")
}

func init() {
	cons := connectionString()
	log.Println("connecting onto:", cons)
	Session, err := mgo.Dial(cons)
	Session.SetMode(mgo.Monotonic, true)
	if err != nil {
		log.Panic("can't init db module: ", err)
	}

	DB = Session.DB(os.Getenv("MONGO_DB"))
	Sessions = DB.C("sessions")
	users = DB.C("users")
	groups = DB.C("groups")
	friendships = DB.C("friendships")
	memberships = DB.C("memberships")
}

type Model interface {
	GetCollection() *mgo.Collection
	GetID() *bson.ObjectId
}
