package db

import (
	"errors"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type Membership struct {
	ID         bson.ObjectId `bson:"_id,omitempty"`
	GroupID    bson.ObjectId `bson:"group,omitempty"`
	InvitedID  bson.ObjectId `bson:"invited,omitempty"`
	InvitingID bson.ObjectId `bson:"inviting,omitempty"`
	Role       string        `bson:"role"`
	History    []State       `bson:"history"`
}

type filterF func(Membership) bool

func (m *Membership) GetCollection() *mgo.Collection {
	return memberships
}

func (m *Membership) GetID() *bson.ObjectId {
	return &m.ID
}

func (m *Membership) GetHistory() *History {
	return &History{m, m.History}
}

func (m *Membership) Remove(by *User) error {
	return m.GetHistory().Push(REMOVED, by)
}

func (m *Membership) Confirm(by *User) error {
	return m.GetHistory().Push(CONFIRMED, by)
}

func FindAllInvited(user *User) ([]Membership, error) {
	return findMemberships(user, "invited")
}

func FindAllInviting(user *User) ([]Membership, error) {
	return findMemberships(user, "inviting")
}

func FindAllByGroup(group *Group) ([]Membership, error) {
	return findMemberships(group, "group")
}

func FindMembership(id string) (*Membership, error) {
	if !bson.IsObjectIdHex(id) {
		return nil, errors.New("invalid id")
	}

	var ms Membership
	err := memberships.FindId(bson.ObjectIdHex(id)).One(&ms)

	if err != nil {
		return nil, err
	}

	return &ms, nil
}

func findMemberships(model Model, field string) ([]Membership, error) {
	query := make([]Membership, 0)

	err := memberships.Find(bson.M{field: model.GetID()}).All(&query)
	if err != nil {
		return nil, err
	}

	return query, nil
}

func FilterByLambda(filter filterF, ms []Membership) []Membership {
	results := make([]Membership, 0)

	for _, _ms := range ms {
		if filter(_ms) {
			results = append(results, _ms)
		}
	}

	return results
}

func FilterByStatus(status string, ms []Membership) []Membership {
	return FilterByLambda(func(_ms Membership) bool {
		return _ms.GetHistory().Latest().Status == status
	}, ms)
}

func (m *Membership) Marshall() map[string]string {
	myMap := make(map[string]string)

	//myMap["role"] = m.Role
	//myMap["status"] = m.GetHistory().Latest().Status
	myMap["invited"] = m.InvitedID.Hex()
	myMap["inviting"] = m.InvitingID.Hex()
	myMap["group"] = m.GroupID.Hex()
	myMap["id"] = m.ID.Hex()

	return myMap
}

func insertMembership(
	group *Group,
	invitingMember *User,
	invited *User,
	role string,
	status string,

) (*Membership, error) {

	id := bson.NewObjectId()
	history := State{status, currentDate(), invitingMember.ID}

	req := &Membership{
		id,
		group.ID,
		invited.ID,
		invitingMember.ID,
		role,
		[]State{history},
	}

	if err := memberships.Insert(req); err != nil {
		return nil, err
	}

	return req, nil
}
