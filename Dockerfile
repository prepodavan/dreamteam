FROM golang:1.12.1

RUN go get -u github.com/golang/dep/cmd/dep

WORKDIR /go/src/dreamteam
COPY . ./
RUN dep ensure

RUN go test -v
RUN go build app.go

CMD [ "./app" ]