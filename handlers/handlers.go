package handlers

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func responseOk(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "ok"})
}

func responseError(c *gin.Context, code int, msg string) {
	c.AbortWithStatusJSON(code, gin.H{"error": msg})
}

func responseError500(c *gin.Context, e error) {
	log.Println(e)
	c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "internal error"})
}

func responseNotJsonFormat(c *gin.Context) {
	c.JSON(http.StatusBadRequest, gin.H{"error": "wrong format"})
}
