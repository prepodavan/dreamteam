package handlers

import (
	"dreamteam/db"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Auth(c *gin.Context) {
	userJson := c.Keys["userJson"].(map[string]string)

	dbUser, err := db.FindUserByName(userJson["name"])
	if err != nil {
		errStr := err.Error()
		if errStr == "not found" || errStr == "invalid id" || dbUser == nil {
			responseError(c, http.StatusNotFound, "not found")
			return
		}
		responseError500(c, err)
		return
	}

	if dbUser.Password != userJson["password"] {
		responseError(c, http.StatusForbidden, "wrong input")
		return
	}

	session := sessions.Default(c)
	session.Set("uid", dbUser.ID.Hex())
	session.Save()
	responseOk(c)
}

func RequiresNameAndPass(c *gin.Context) {
	user := make(map[string]string)

	err := c.BindJSON(&user)
	if err != nil {
		responseNotJsonFormat(c)
		return
	}

	_, withName := user["name"]
	_, withPass := user["password"]

	if !withName || !withPass {
		responseError(c, http.StatusBadRequest, "requires name and password")
		return
	}

	c.Keys["userJson"] = user
}

func findUserOrDrop(c *gin.Context, uid string, code int, msg string) *db.User {
	user, err := db.FindUser(uid)
	if err != nil {
		errStr := err.Error()
		if errStr == "not found" || errStr == "invalid id" {
			responseError(c, code, msg)
			return nil
		}
		responseError500(c, err)
		return nil
	}

	return user
}

func CheckAuth(c *gin.Context) {
	session := sessions.Default(c)
	checkUID := session.Get("uid")
	if checkUID == nil {
		responseError(c, http.StatusUnauthorized, "unauthorized")
		return
	}

	user := findUserOrDrop(c, checkUID.(string), http.StatusUnauthorized, "unauthorized")
	if user == nil {
		return
	}

	c.Keys["user"] = user
}
