package handlers

import (
	"dreamteam/db"
	"github.com/gin-gonic/gin"
	"net/http"
	"path"
	"strings"
)

func RequiresTitleAndDesc(c *gin.Context) {
	groupInfo := make(map[string]string)

	err := c.BindJSON(&groupInfo)
	if err != nil {
		responseNotJsonFormat(c)
		return
	}

	title, withTitle := groupInfo["title"]
	_, withDescription := groupInfo["description"]

	if !withTitle || !withDescription || strings.Trim(title, " \n\t\r") == "" {
		responseError(c, http.StatusBadRequest, "requires groupInfo")
		return
	}

	c.Keys["groupInfo"] = groupInfo
}

func CreateGroup(c *gin.Context) {
	user := c.Keys["user"].(*db.User)
	groupInfo := c.Keys["groupInfo"].(map[string]string)

	gr, err := user.CreateGroup(groupInfo["title"], groupInfo["description"])
	if err != nil {
		responseError500(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"id": gr.ID.Hex()})
}

func RequiresGroup(c *gin.Context) {
	groupID := c.Param("id")

	gr, err := db.FindGroup(groupID)
	if err != nil {
		errStr := err.Error()
		if errStr == "not found" {
			responseError(c, http.StatusNotFound, errStr)
			return
		}

		if errStr == "invalid id" {
			responseError(c, http.StatusBadRequest, errStr)
			return
		}

		responseError500(c, err)
		return
	}

	c.Keys["group"] = gr
}

func GetGroup(c *gin.Context) {
	group := c.Keys["group"].(*db.Group)

	c.JSON(http.StatusOK, gin.H{"title": group.Title, "id": group.ID.Hex(), "description": group.Description})
}

func CheckIsMember(c *gin.Context) {
	user := c.Keys["user"].(*db.User)
	group := c.Keys["group"].(*db.Group)

	groups, err := user.Groups()
	if err != nil {
		responseError500(c, err)
	}

	var membership *db.Membership
	for _, gr := range groups {
		if gr.GroupID.Hex() == group.ID.Hex() {
			membership = &gr
			break
		}
	}

	if membership == nil {
		responseError(c, http.StatusForbidden, "u r not a member of this group")
		return
	}

	c.Keys["membership"] = membership
}

func GetMembers(c *gin.Context) {
	group := c.Keys["group"].(*db.Group)

	members, err := group.Users()
	if err != nil {
		responseError500(c, err)
		return
	}

	response := make([]map[string]string, 0)

	for _, m := range members {
		memberMap := make(map[string]string)
		memberMap["id"] = m.InvitedID.Hex()
		response = append(response, memberMap)
	}

	c.JSON(http.StatusOK, gin.H{"members": response})
}

func SetImage(imgDir string) gin.HandlerFunc {
	return func(c *gin.Context) {
		group := c.Keys["group"].(*db.Group)

		file, err := c.FormFile("file")
		if err != nil {
			responseError(c, http.StatusBadRequest, "requires image file")
			return
		}

		err = c.SaveUploadedFile(file, path.Join(imgDir, group.ID.Hex()))

		if err != nil {
			responseError500(c, err)
			return
		}
		responseOk(c)
	}
}

func GetImage(imgUrl string, router *gin.Engine) gin.HandlerFunc {
	return func(c *gin.Context) {
		group := c.Keys["group"].(*db.Group)

		var url string
		if imgUrl[len(imgUrl)-1] == '/' {
			url = imgUrl + group.ID.Hex()
		} else {
			url = imgUrl + "/" + group.ID.Hex()
		}

		c.Request.URL.Path = url
		router.HandleContext(c)
	}
}
