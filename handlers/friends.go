package handlers

import (
	"dreamteam/db"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func GetFriends(c *gin.Context) {
	user := c.Keys["user"].(*db.User)

	friends, err := user.Friends()
	if err != nil {
		responseError500(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"friends": friends})
}

func GetRequests(c *gin.Context) {
	user := c.Keys["user"].(*db.User)

	reqs, err := user.Requests()
	if err != nil {
		responseError500(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"requests": reqs})
}

func RequiresPersonID(c *gin.Context) {
	person := make(map[string]string)

	err := c.BindJSON(&person)
	if err != nil {
		responseNotJsonFormat(c)
		return
	}

	id, withID := person["id"]

	if !withID || strings.Trim(id, " \r\n\t") == "" {
		responseError(c, http.StatusBadRequest, "requires request person's id")
		return
	}

	c.Keys["personID"] = id
}

func CheckPersonID(c *gin.Context) {
	personID := c.Keys["personID"].(string)

	person, err := db.FindUser(personID)
	if err != nil {
		errStr := err.Error()
		if errStr == "not found" || errStr == "invalid id" || person == nil {
			responseError(c, http.StatusBadRequest, "wrong person's id")
			return
		}
		responseError500(c, err)
		return
	}

	c.Keys["person"] = person
}

func ConfirmFriendHandler(c *gin.Context) {
	user := c.Keys["user"].(*db.User)
	person := c.Keys["person"].(*db.User)

	if err := user.ConfirmFriend(person); err != nil {
		errStr := err.Error()
		if errStr == "not found" || errStr == "nothing to confirm" {
			responseError(c, http.StatusBadRequest, "request does not exist")
			return
		}

		responseError500(c, err)
		return
	}

	responseOk(c)
}

func RequestForFriendship(c *gin.Context) {
	user := c.Keys["user"].(*db.User)
	person := c.Keys["person"].(*db.User)

	if err := user.RequestFriend(person); err != nil {
		errStr := err.Error()
		if errStr == "request already exists" || errStr == "invalid recipient" {
			responseError(c, http.StatusForbidden, errStr)
			return
		}
		responseError500(c, err)
		return
	}

	responseOk(c)
}

func RemoveFriendHandler(c *gin.Context) {
	user := c.Keys["user"].(*db.User)
	person := c.Keys["person"].(*db.User)

	err := user.RemoveFriend(person)
	if err != nil {
		errStr := err.Error()
		if errStr == "not found" || errStr == "nothing to remove" {
			responseError(c, http.StatusBadRequest, "person does not in friend list")
			return
		}
		responseError500(c, err)
		return
	}

	responseOk(c)
}

func IsFriendWithPerson(c *gin.Context) {
	user := c.Keys["user"].(*db.User)
	person := c.Keys["person"].(*db.User)

	ids, err := user.Friends()
	if err != nil {
		responseError500(c, err)
		return
	}

	flag := false
	for _, id := range ids {
		if id == person.ID.Hex() {
			flag = true
			break
		}
	}

	if !flag {
		responseError(c, http.StatusBadRequest, "u r not friends")
		return
	}
}
