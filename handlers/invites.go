package handlers

import (
	"dreamteam/db"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func RequiresGroupAndPerson(c *gin.Context) {
	request := make(map[string]string)

	err := c.BindJSON(&request)
	if err != nil {
		responseNotJsonFormat(c)
		return
	}

	invitedID, withInvited := request["invited_id"]
	groupID, withGroup := request["group_id"]

	if !withInvited || strings.Trim(invitedID, " \r\n\t") == "" {
		responseError(c, http.StatusBadRequest, "requires invited person's id")
		return
	}

	if !withGroup || strings.Trim(groupID, " \r\n\t") == "" {
		responseError(c, http.StatusBadRequest, "requires group's id")
		return
	}

	gr, err := db.FindGroup(groupID)
	if err != nil {
		errStr := err.Error()
		if errStr == "group not found" {
			responseError(c, http.StatusNotFound, errStr)
			return
		}

		if errStr == "invalid group's id" {
			responseError(c, http.StatusBadRequest, errStr)
			return
		}

		responseError500(c, err)
		return
	}

	c.Keys["group"] = gr
	c.Keys["personID"] = invitedID

}

func Invite(c *gin.Context) {
	user := c.Keys["user"].(*db.User)
	group := c.Keys["group"].(*db.Group)
	friend := c.Keys["person"].(*db.User)

	membership, err := user.Invite(group, friend)

	if err != nil {

		errStr := err.Error()

		if errStr == "already exists" || errStr == "already member" || errStr == "not a friend" {
			responseError(c, http.StatusForbidden, errStr)
			return
		}

		responseError500(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"id": membership.ID.Hex()})
}

func GetInvites(c *gin.Context) {
	user := c.Keys["user"].(*db.User)

	inviting, err := db.FindAllInviting(user)
	if err != nil {
		responseError500(c, err)
		return
	}

	invited, err := db.FindAllInvited(user)
	if err != nil {
		responseError500(c, err)
		return
	}

	invitingResponse := make([]map[string]string, 0)
	invitedResponse := make([]map[string]string, 0)

	for _, ms := range db.FilterByStatus(db.PENDING, inviting) {
		invitingResponse = append(invitingResponse, ms.Marshall())
	}

	for _, ms := range db.FilterByStatus(db.PENDING, invited) {
		invitedResponse = append(invitedResponse, ms.Marshall())
	}

	c.JSON(http.StatusOK, gin.H{"outgoing": invitingResponse, "incoming": invitedResponse})
}

func RequireInviteID(c *gin.Context) {
	inviteID := c.Param("id")
	ms, err := db.FindMembership(inviteID)
	if err != nil {
		if errStr := err.Error(); errStr == "not found" || errStr == "invalid id" {
			responseError(c, http.StatusNotFound, "not found")
			return
		}

		responseError500(c, err)
		return
	}

	c.Keys["invite"] = ms
}

func DenyInvite(c *gin.Context) {
	user := c.Keys["user"].(*db.User)
	invite := c.Keys["invite"].(*db.Membership)

	err := user.DenyInvite(invite)
	if err != nil {
		if errStr := err.Error(); errStr == "not found" {
			responseError(c, http.StatusNotFound, errStr)
			return
		}

		responseError500(c, err)
		return
	}

	responseOk(c)
}

func ConfirmInvite(c *gin.Context) {
	user := c.Keys["user"].(*db.User)
	invite := c.Keys["invite"].(*db.Membership)

	err := user.ConfirmInvite(invite)
	if err != nil {
		if errStr := err.Error(); errStr == "not found" {
			responseError(c, http.StatusNotFound, errStr)
			return
		}

		responseError500(c, err)
		return
	}

	responseOk(c)
}
