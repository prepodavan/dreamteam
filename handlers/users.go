package handlers

import (
	"dreamteam/db"
	"github.com/gin-gonic/gin"
	"net/http"
)

func CheckUserAlreadyExists(c *gin.Context) {
	user := c.Keys["userJson"].(map[string]string)

	dbUser, err := db.FindUserByName(user["name"])
	if err != nil && err.Error() != "not found" {
		responseError500(c, err)
		return
	}

	if dbUser != nil {
		responseError(c, http.StatusForbidden, "already exists")
		return
	}
}

func PostUser(c *gin.Context) {
	user := c.Keys["userJson"].(map[string]string)

	dbUser, err := db.CreateUser(user["name"], user["password"])
	if err != nil {
		responseError500(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"id": dbUser.ID.Hex()})
}

func CheckUserID(c *gin.Context) {
	user := findUserOrDrop(c, c.Param("id"), http.StatusNotFound, "not found")
	if user == nil {
		return
	}

	c.Keys["gettingUser"] = user
}

func GetUser(c *gin.Context) {
	user := c.Keys["gettingUser"].(*db.User)

	c.JSON(http.StatusOK, gin.H{"id": user.ID.Hex(), "name": user.Name})
}

func GetGroups(c *gin.Context) {
	user := c.Keys["gettingUser"].(*db.User)

	memberships, err := user.Groups()
	if err != nil {
		responseError500(c, err)
		return
	}

	response := make([]map[string]string, 0)
	for _, ms := range memberships {
		msMap := make(map[string]string)

		id := ms.GroupID.Hex()

		flag := false
		for _, group := range response {
			if group["id"] == id {
				flag = true
				break
			}
		}

		if !flag {
			msMap["id"] = id
			response = append(response, msMap)
		}
	}

	c.JSON(http.StatusOK, gin.H{"groups": response})
}
